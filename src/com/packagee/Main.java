package com.packagee;

public class Main {

    public static void main(String[] args) {
        StackOperations stack=new Stack();
        stack.push("1");
        stack.push("2");
        stack.push("3");
        System.out.println(stack.get().toString());
        System.out.println("Top element: "+stack.pop());
        System.out.println(stack.get().toString());
        stack.pop();
        stack.pop();
        //now stack is empty
        System.out.println(stack.pop());
        System.out.println(stack.get());
    }
}
