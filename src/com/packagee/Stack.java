package com.packagee;

import java.util.*;

public class Stack implements StackOperations {
    List<String> stack=new LinkedList<>();

    @Override
    public List<String> get() {
        if(stack.isEmpty()){
            return Collections.EMPTY_LIST;
        }
        else{
            return stack;
        }
    }

    @Override
    public Optional<String> pop() {
        if(stack.isEmpty()){
            return Optional.empty();
        }
        else{
            String item=stack.get(0);
            stack.remove(0);
            return Optional.of(item);
        }
    }

    @Override
    public void push(String item) {
        stack.add(0,item);
    }
}
