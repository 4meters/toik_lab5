package com.packagee;

import java.util.List;
import java.util.Optional;

public interface StackOperations {
    //gets all elements on stack - first element is stack top
    List<String> get();

    //method for getting element from stack top
    Optional<String> pop();

    //method for adding element to stack
    void push(final String item);
}
